FROM ubuntu:latest
RUN \
  apt-get -y update && \
  apt-get install -y software-properties-common && \
  add-apt-repository ppa:openjdk-r/ppa && \
  apt-get -y update && \
  apt-get install -y openjdk-8-jdk && \
  apt-get install -y wget && \
  apt-get install mysql-server-core-5.7 mysql-server-5.7 -y &&\
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*
COPY ./my.cnf /etc/mysql/my.cnf
RUN mkdir /var/run/mysqld
WORKDIR /var/log/mysql
RUN chown mysql:mysql /var/run/mysqld
RUN mysqld &
RUN export JAVA_HOME="/usr/bin/java"
WORKDIR /scripts
COPY jenkins.war ./
EXPOSE 8080
EXPOSE 3036
COPY ./run.sh /scripts
RUN chmod +x /scripts/run.sh
ENTRYPOINT ["sh","-c"]
CMD ["java -jar /scripts/jenkins.war"]
